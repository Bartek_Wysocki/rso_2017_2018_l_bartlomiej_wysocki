﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.Serialization;

namespace BookService
{
    [DataContract]
    public class BookInfo
    {
        [DataMember]
        string title;

        [DataMember]
        string author;


        [DataMember]
        private DateTime borrowDate;
        [DataMember]
        private DateTime internalReturnDate;

        [DataMember]
        Status status = new Status();



        public DateTime BorrowDate { get => borrowDate; set => borrowDate = value; }
        public DateTime InternalReturnDate { get => internalReturnDate; set => internalReturnDate = value; }
        public Status Status { get => status; set => status = value; }
        

        public BookInfo() { }

        public BookInfo(string title, string author)
        {
            this.title = title;
            this.author = author;
        }


        
    }
}
