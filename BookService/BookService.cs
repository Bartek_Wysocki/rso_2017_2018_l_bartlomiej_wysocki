﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace BookService
{
    // UWAGA: możesz użyć polecenia „Zmień nazwę” w menu „Refaktoryzuj”, aby zmienić nazwę klasy „BookService” w kodzie i pliku konfiguracji.
    public class BookService : IBookService
    {
        public static List<Book> booksList = new List<Book>() {
        new Book(new BookInfo("Book1","Author1")),
        new Book(new BookInfo("Book2","Author2")),
        new Book(new BookInfo("Book3","Author3")),
        new Book(new BookInfo("Book4","Author4")),
        new Book(new BookInfo("Book5","Author5")),
        new Book(new BookInfo("Book6","Author6")),
        new Book(new BookInfo("Book7","Author7"))
        
        };
        


        public List<Book> BooksList { get => booksList; set => booksList = value; }


        public bool addBook(Book book)
        {
            booksList.Add(book);
            return true;
        }



        public Book getBook(int bookID) {
            foreach (Book book in booksList) {
                if (book.BookID == bookID)
                {
                    return book;
                }
                
            }
            return null;
        }

        public Status borrowBook(int userID,int bookID)
        {
            Book book = getBook(bookID);
            if(book.BookInfo.Status.Reserved == false)
            {
                book.UserID = userID;
                book.ReturnDate = book.BookInfo.InternalReturnDate = new DateTime().AddDays(14);
                book.BookInfo.BorrowDate = new DateTime();
                book.BookInfo.Status.Reserved = true;
                return book.BookInfo.Status;
            }
            else
            {
                return new Status();
            }
            
        }

        public List<Book> getAllBooks()
        {
            return booksList;
        }

        public BookInfo getBookInfo(int bookID)
        {
            var book = booksList.Find(item => item.BookID == bookID);
            if(book != null)
            {
                return book.BookInfo;
            }
            else
            {
                return new BookInfo();
            }
        }

        public List<Book> getBorrowedBooks(int userID)
        {
            var books = BooksList.FindAll(item => item.UserID == userID);
            if(books != null)
            {
                return books;
            }
            else
            {
                return new List<Book>();
            }
        }

       

          public List<Book> listBorrowedItems()
        {
            var books = booksList.FindAll(item => item.BookInfo.Status.Reserved == true);
            if(books != null)
            {
                return books;
            }
            else
            {
                return new List<Book>();
            }
        }

     


        

        

    }
}
