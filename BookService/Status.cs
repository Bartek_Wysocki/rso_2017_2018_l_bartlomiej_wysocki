﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.Serialization;

namespace BookService
{   [DataContract]
    public class Status
    {
        [DataMember]
        private bool reserved;


        public Status() { }
        public Status(bool reserved)
        {
            
            this.reserved = reserved;
        }

        public bool Reserved { get => reserved; set => reserved = value; }
    }
}
