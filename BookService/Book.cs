﻿using System;
using System.Runtime.Serialization;
namespace BookService
{
    [DataContract]
    public class Book
    {
        static int count;
        public Book() { }

        public Book(BookInfo bookInfo) {
            BookID = ++Count;
            BookInfo = bookInfo;
        }
        
        [DataMember]
        private int bookID;

        [DataMember]
        private int userID;


        [DataMember]
        private DateTime returnDate;

        [DataMember]
        private Status status;

        [DataMember]
        private BookInfo bookInfo;

        public int BookID { get => bookID; set => bookID = value; }
        public DateTime ReturnDate { get => returnDate; set => returnDate = value; }
        internal BookInfo BookInfo { get => bookInfo; set => bookInfo = value; }
        public Status Status { get => status; set => status = value; }
        public static int Count { get => count; set => count = value; }
        public int UserID { get => userID; set => userID = value; }
    }
}