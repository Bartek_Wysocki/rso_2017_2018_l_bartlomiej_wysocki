#ifndef ENDIANS_H
#define ENDIANS_H
#include <stdint.h>

uint64_t htond(double number);
double ntohd(uint64_t number);
int is_bigendian(void);
#endif