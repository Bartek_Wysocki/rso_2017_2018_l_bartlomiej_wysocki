#include <sys/types.h>
#include <sys/socket.h>
#include <stdio.h>
#include <stdlib.h>
#include <netinet/in.h>
#include <signal.h>
#include <unistd.h>
#include "server.h"
#include <math.h>
#include <time.h>
#include <string.h>
#include "endians.h"


int
main ()
{
	int server_sockfd, client_sockfd;
	socklen_t server_len, client_len;
	struct sockaddr_in server_address;
	struct sockaddr_in client_address;
	int option = 1;
	

	if((server_sockfd = socket (AF_INET, SOCK_STREAM, 0)) == -1){
		perror("socket");
		exit(1);
	}

	setsockopt(server_sockfd, SOL_SOCKET, SO_REUSEADDR, &option, sizeof(option));

	server_address.sin_family = AF_INET;
	server_address.sin_addr.s_addr = htonl (INADDR_ANY);
	server_address.sin_port = htons (9734);
	server_len = sizeof (server_address);
	if(bind (server_sockfd, (struct sockaddr *) &server_address, server_len) == -1){
		perror("bind");
		exit(1);

	}

	/*  Create a connection queue, ignore child exit details and wait for clients.  */

	listen (server_sockfd, 5);




	while (1)
	{
		

		printf ("server waiting\n");

		/*  Accept connection.  */

		client_len = sizeof (client_address);
		client_sockfd = accept (server_sockfd,
				(struct sockaddr *) &client_address,
				&client_len);
		if(client_sockfd == -1){
			perror("accept");

		}

		/*  Fork to create a process for this client and perform double sqrt_number;a test to see
			whether we're the parent or the child.  */

		if (fork () == 0)
		{

			/*  If we're the child, we can now read/write to the client on client_sockfd.
				The five second delay is just for this demonstration.  */



			sleep(5);
			///pierwiastek
			sqrt_pack_response sqrt_response;
			read (client_sockfd, &sqrt_response, sizeof(sqrt_pack_response));
			printf("-----server----\n");
			printf("%c %c %c %c\n",sqrt_response.header[0],sqrt_response.header[1],sqrt_response.header[2],sqrt_response.header[3]);
			double temp_num = ntohd(sqrt_response.number);
			//sscanf(sqrt_response.number,"%lf",&temp_num);
			//temp_num = sqrt_response.number;
			//printf("%f\n",temp_num);
			printf("%lf\n",ntohd(sqrt_response.number));
			printf("---------------\n");
			//double sqrt_number_temp;
			//sqrt_number_temp = sqrt((float)temp_num);
			//sqrt_number_temp =sqrt((float)temp_num);
			sqrt_response.header[0] = '1';
			sqrt_response.number = htond(sqrt(temp_num));
			//sprintf(sqrt_response.number,"%lf",sqrt_number_temp);
			write(client_sockfd,&sqrt_response,sizeof(sqrt_pack_response));
			
			///data i czas
			date_pack_response date_response;
			read(client_sockfd,&date_response,sizeof(date_pack_response));
			printf("-----server----\n");
			printf("%c %c %c %c\n",date_response.header[0],date_response.header[1],date_response.header[2],date_response.header[3]);
			printf("---------------\n");
			printf("\n");
			printf("\n");
			printf("\n");

			//czas systemu
			time_t mytime = time(NULL);
			const char* currentTime = ctime(&mytime);

			strcpy(date_response.date,currentTime);
			date_response.header[0] = '1';
			write(client_sockfd,&date_response,sizeof(date_response));
			

			//ch++;
			//write (client_sockfd, &ch, 1);
			close (client_sockfd);
			exit (0);
		}

		/*  Otherwise, we must be the parent and our work for this client is finished.  */

		else
		{
			close (client_sockfd);
		}
	}
}

