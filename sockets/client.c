/*  Make the necessary includes and set up the variables.  */

#include <sys/types.h>
#include <sys/socket.h>
#include <stdio.h>
#include <stdlib.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <unistd.h>
#include "client.h"
#include "endians.h"





int
main ()
{
	int sockfd;
	socklen_t len;
	struct sockaddr_in address;
	int result;

	/*  Create a socket for the client.  */

	sockfd = socket (AF_INET, SOCK_STREAM, 0);

	/*  Name the socket, as agreed with the server.  */

	address.sin_family = AF_INET;
	address.sin_addr.s_addr = inet_addr ("127.0.0.1");
	address.sin_port = htons (9734);
	len = sizeof (address);

	/*  Now connect our socket to the server's socket.  */

	result = connect (sockfd, (struct sockaddr *) &address, len);

	if (result == -1)
	{
		perror ("oops: netclient");
		exit (1);
	}

	/*  We can now read/write via sockfd.  */

	double number = 16;
	
	

	sqrt_pack sqrt_pack_message;

	sqrt_pack_message.header[0] = '0';
	sqrt_pack_message.header[1] = '0';
	sqrt_pack_message.header[2] = '0';
	sqrt_pack_message.header[3] = '1';
	sqrt_pack_message.request_id = '1';
	//sprintf(sqrt_pack_message.number,"%lf",number);
	//printf("%s\n",sqrt_pack_message.number);
	sqrt_pack_message.number = htond(number);


	date_pack date_pack_message;
	
	date_pack_message.header[0] = '0';
	date_pack_message.header[1] = '0';
	date_pack_message.header[2] = '0';
	date_pack_message.header[3] = '2';
	date_pack_message.request_id = '2';


	//write (sockfd, &ch, 1);
	//read (sockfd, &ch, 1);
	//printf ("char from server = %d\n", ch);

	write(sockfd,&sqrt_pack_message,sizeof(struct sqrt_pack));

	read (sockfd, &sqrt_pack_message, sizeof(struct sqrt_pack));
			printf("-----client----\n");
			printf("%c %c %c %c\n",sqrt_pack_message.header[0],sqrt_pack_message.header[1],sqrt_pack_message.header[2],sqrt_pack_message.header[3]);
			double temp_num;
			//sscanf(sqrt_pack_message.number,"%lf",&temp_num);
			//temp_num = ntohd(sqrt_pack_message.number);
			printf("%f\n",ntohd(sqrt_pack_message.number));
			printf("---------------\n");
			
		


	write(sockfd,&date_pack_message,sizeof(struct date_pack));
	
	read(sockfd,&date_pack_message,sizeof(struct date_pack));
	printf("-----client----\n");
	printf("%c %c %c %c\n",date_pack_message.header[0],date_pack_message.header[1],date_pack_message.header[2],date_pack_message.header[3]);
	printf("%s\n",date_pack_message.date);
	printf("---------------\n");
	printf("\n");
	printf("\n");
	printf("\n");



	close (sockfd);
	exit (0);
}


