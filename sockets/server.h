#ifndef SERVER_H
#define SERVER_H

typedef struct sqrt_pack_response{
    char header[4];
    char request_id;
    double number;
} sqrt_pack_response;

typedef struct date_pack_response {
    char header[4];
    char request_id;
    char date[32];
} date_pack_response;



#endif