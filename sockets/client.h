#ifndef CLIENT_H
#define CLIENT_H

typedef struct sqrt_pack {
    char header[4];
    char request_id;
    double number;

} sqrt_pack;


typedef struct date_pack {
    char header[4];
    char request_id;
    char date[32];
} date_pack;

#endif