#include <stdint.h>
#include "endians.h"


uint64_t htond(double number){
    char *data = (char*)&number;
    uint64_t result;
    char *dest=(char *)&result;
    if(!is_bigendian()){
        for(int i=0;i<sizeof(double);i++){
            dest[i] = data[sizeof(double)-i- 1];
             }
        }else {
            for(int i=0;i<sizeof(double);i++){
                dest[i]=data[i];
            }
    
    }
    return result;
}




double ntohd(uint64_t number){
    char *data = (char*)&number;
    double result;
    char *dest=(char *)&result;
    if(!is_bigendian()){
        for(int i=0;i<sizeof(double);i++){
            dest[i] = data[sizeof(double)-i-1];
             }
        }else {
            for(int i=0;i<sizeof(double);i++){
                dest[i]=data[i];
            }
    
    }
    return result;
}


int is_bigendian(){
    int num = 1;
    char *ptr;
    ptr = (char*)&num;
    return (*ptr);
}