#include <Ice/Ice.h>
#include <Printer.h>


using namespace std;
using namespace Demo;

int
main(int argc, char* argv[])
{
    try
    {
        Ice::CommunicatorHolder ich(argc, argv);
        Ice::ObjectPrx base = ich->stringToProxy("SimplePrinter:default -p 10000");
        PrinterPrx printer = PrinterPrx::checkedCast(base);
        if(!printer)
        {
            throw "Invalid proxy";
        }
        string s;
        while(1){

            cout<< "Podaj slowo\n";
            cin >> s;
            string word = printer->change(s);
            cout << word << endl;

        }
    } 
    catch(const std::exception& ex)
    {
        cerr << ex.what() << endl;
        return 1;
    }
    return 0;
}
