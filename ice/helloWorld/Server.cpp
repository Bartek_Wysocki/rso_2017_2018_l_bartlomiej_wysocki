#include <Ice/Ice.h>
#include <Printer.h>

using namespace std;
using namespace Demo;

class PrinterI : public Printer
{
public:
    virtual string change(const string& s, const Ice::Current&);
};

string
PrinterI::
change(const string& s, const Ice::Current&)
{
    string word = s;
    word[4]='X';
    return word;
    
}

int
main(int argc, char* argv[])
{
    try
    {
        Ice::CommunicatorHolder ich(argc, argv);
        Ice::ObjectAdapterPtr adapter =
            ich->createObjectAdapterWithEndpoints("SimplePrinterAdapter", "default -p 10000");
        Ice::ObjectPtr object = new PrinterI;
        adapter->add(object, ich->stringToIdentity("SimplePrinter"));
        adapter->activate();
        ich->waitForShutdown();
    } 
    catch(const std::exception& e)
    {
        cerr << e.what() << endl;
        return 1;
    }
    return 0;
}
