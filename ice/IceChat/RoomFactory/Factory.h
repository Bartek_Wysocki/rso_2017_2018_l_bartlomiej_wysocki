#ifndef FACTORY_H
    #define FACTORY_H

    #include <Ice/Ice.h>
    #include "chat.h"
    #include "RoomFactoryI.h"
    #include "Ports.h"

    using namespace std;
    using namespace Chat;
    using namespace Interfaces;

    namespace RoomFactory {
        class Factory {
            public:
                Factory();
                void registerRoomFactory();
                ~Factory();
            private:
                ServerPrx server;
                Ice::CommunicatorPtr iceCommunicator;
                Ice::ObjectAdapterPtr adapter;
                RoomFactoryPrx roomFactory;
                Ports portsUtil;
        };
    }

#endif