#ifndef CLIENT_H
    #define CLIENT_H

    #include <thread>
    #include <Ice/Ice.h>
    #include "chat.h"
    #include "UserI.h"
    #include "Ports.h"
    
    using namespace std;
    using namespace Chat;
    using namespace Interfaces;

    namespace ClientApp {
        class Client {
            public:
                Client(const string&);
                ~Client();

                void joinRoom();
                void leaveRoom();
                void createRoom() const;
                void listRooms() const;
                
                
                void printUsersInRoom() const;
                
                void sendPrivateMessage() const;
                void sendMessageToRoom() const;
                
            private:
                string username;
                UserPrx user;
                ServerPrx server;
                //ptr uchwyt
                Ice::CommunicatorPtr iceCommunicator;
                Ice::ObjectAdapterPtr adapter;
                Ports portsUtil;
                RoomList userRooms;
                void createUser();
                string getNameOfTheRoom() const;
                
                UserList getUsersInRoom() const;
        }; 
    }

#endif