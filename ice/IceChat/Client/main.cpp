#include "Client.h"
#include "ServerI.h"


using namespace std;
using namespace ClientApp;





int getChoice() {
    int c;
        cin >> c; 
        while (c>7 || c<0) {
            cout << "Wybierz poprawnie" << endl;
            cin >> c;
        }
    return c;
}

void printMenu() {
    cout << endl;
    cout << "1.Dolacz do pookoju" << endl;
    cout << "2.Dostepne pokoje" << endl;
    cout << "3.Stworz pokoj" << endl;
    cout << "4.Wyjdz z pokoju" << endl;
    cout << "5.Wyslij wiadomosc" << endl;
    cout << "6.Pokaz uzytkownikow" << endl;
    cout << "7.Wyslij prywatna" << endl;
    cout << "0.Koniec" << endl;
    cout << endl;
}

int main(int argc, char* argv[]) {
    
    string name;
    int s = 0;


    cout << "podaj nick " << endl;
    cin >> name;


    Client client(name);

    while (true) {
        printMenu();
        int c = getChoice();
        switch (c) {
            case 1:
                client.joinRoom();
                break;
            case 2:
                client.listRooms();
                break;
            case 3:
                client.createRoom();
                break;
            case 4:
                client.leaveRoom();
                break;
            case 5:
                client.sendMessageToRoom();
                break;
            case 6:
                client.printUsersInRoom();
                break;
            case 7:
                client.sendPrivateMessage();
                break;
            case 0:
                return s;
        }
    }

    return s;
}