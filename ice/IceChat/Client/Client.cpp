#include "Client.h"

namespace ClientApp {

     Client::Client(const string& name) : username(name){
        try {
            //tworzy obiekt Ice
            iceCommunicator = Ice::initialize();
            //pobieram port z pliku naglowkowego
            int port = portsUtil.getServerPort();
            //tworzy proxy o danej nazwie i porcie z pliku naglowkeog
            Ice::ObjectPrx base = iceCommunicator->stringToProxy("Server:default -p " + to_string(port));
            //rzutuje obiekt klasy bazowej na typ klasy pochodnej ze sprawdzaniem
            server = ServerPrx::checkedCast(base);
            if (!server)
                throw "Invalid proxy";
        } catch (const Ice::Exception& ex) {
            cerr << ex << endl;
        } 
        createUser();
    }


    //tworzenie uzytkownika
    void Client::createUser() {
        UserPtr object = new UserI(username);
        //zwraca losowy port z danego przedzialu
        int port = portsUtil.getRandomPort();
        //tworzy adapter do dla uzytkownika o danym porcie
        adapter = iceCommunicator->createObjectAdapterWithEndpoints("User" + username, "default -p " + to_string(port));
        //rzutuje obiekt klasy bazowej na typ klasy pochodnej bez sprawdzania
        user = UserPrx::uncheckedCast(adapter->addWithUUID(object));
        adapter->activate();
    }

   
    //destruktor
    Client::~Client() {
        //auto typ ktory zostanie wydedukowany ze zmiennej
        for (auto &room : userRooms) {
            room->LeaveRoom(user);
        }
        if (iceCommunicator)
            iceCommunicator->destroy();
    }

    //const nie zmieniaja sie danego przechowywane przez obiekt
    void Client::createRoom() const {
        string roomName;
        cout << "Wprowadz nazwe pokoju" << endl;
        cin >> roomName;
        //ignore aby wyczyscic stream z niebpotrzebnych znakow nowej lini
        cin.ignore(1000, '\n');
        try {
            server->CreateRoom(roomName);
        } catch (const Chat::RoomAlreadyExist& ex) {
            cerr << ex << endl;
        } catch (const Chat::NoResourcesAvailable& ex) {
            cerr << ex << endl;
        } catch (const Ice::UnknownException& ex) {
            cerr << ex << endl;
        }
    }

    void Client::listRooms() const {
        auto rooms = server->getRooms();
        for (auto room : rooms) {
            cout << room->getName() << endl;
        }
        cout << endl;
    }

    void Client::joinRoom() {
        string name = getNameOfTheRoom();
        try {
            //prxy pokoju
            RoomPrx room = server->FindRoom(name);
            room->AddUser(user);
            userRooms.push_back(room);
        } catch (const NoSuchRoomExist& ex) {
            cerr << ex << endl;
        } catch (const UserAlreadyExists& ex) {
            cerr << ex << endl;
        } catch (const Ice::UnknownException& ex) {
            cerr << ex << endl;
        }
    }

    void Client::sendMessageToRoom() const {
        string room = getNameOfTheRoom();
        for (auto i=  userRooms.begin(); i!= userRooms.end(); ++i) {
            if ((*i)->getName() == room) {
                try {
                    string content;
                    cout << "Wprowadz wiadomosc " << endl;
                    getline(cin, content);
                    (*i)->SendMessage(user, content);
                    return;
                } catch (NoSuchUserExist& ex) {
                    cerr << ex << endl;
                    return;
                } 
            }
        }
        cout << "Wiadomosc zostala wyslana: " << room << endl;
    }

    void Client::printUsersInRoom() const {
        try {
            auto users = getUsersInRoom();
            for (auto& user : users) {
                cout << user->getName() << endl;
            }
        } catch (const Ice::UnknownException& ex) {
            cerr << ex << endl;
        }
    }

    UserList Client::getUsersInRoom() const {
        string roomName = getNameOfTheRoom();
        try {
            //proxy pokoju
            RoomPrx room = server->FindRoom(roomName);
            UserList users = room->getUsers();
            return users;
        } catch (const NoSuchRoomExist& ex) {
            cerr << ex << endl;
        } catch (Ice::UnknownException& ex) {
            cerr << ex << endl;
        }
        return UserList();
    }

    string Client::getNameOfTheRoom() const {
        string roomName;
        cout << "Wprowadz nazwe pokoju" << endl;
        cin >> roomName;
        cin.ignore(1000, '\n');
        return roomName;
    }

    void Client::leaveRoom() {
        string roomName = getNameOfTheRoom();
        for (auto i =  userRooms.begin(); i != userRooms.end(); ++i) {
            if ((*i)->getName() == roomName) {
                try {
                    (*i)->LeaveRoom(user);
                    cout << "Wychodzisz z pokoju " << roomName << endl;
                    userRooms.erase(i);
                    return;
                } catch (NoSuchUserExist& ex) {
                    cerr << ex << endl;
                } catch (Ice::UnknownException& ex) {
                    cerr << ex << endl;
                }
            }
        }
        cout << "Jestes w pokoju " << roomName << endl;
    }



    void Client::sendPrivateMessage() const {
        UserList available;
        
        string person;
        
        try {
            available = getUsersInRoom();
        } catch (NoSuchRoomExist& ex) {
            cerr << ex << endl;
            return;
        }
        cout << "Podaj nick uztykownika" << endl;
        cin >> person;
        
        
        
        cin.ignore(1000, '\n');




        for(auto& p : available) {
            if (p->getName() == person) {
                string message;
                
                
                cout << "Wpisz wiadomosc" << endl;
                //wczytuje do zmiennej message ze strumienia wejscia
                getline(cin, message);
                p->SendPrivateMessage(user, message);
                
                return;
            }
        }
        cout << "Nie ma takiego uzytkwoniak" << endl;
    }

    
}