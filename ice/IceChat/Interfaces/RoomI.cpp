#include "RoomI.h"

namespace Interfaces {
    string RoomI::getName(const ::Ice::Current&) {
        return roomName;
    }

    void RoomI::LeaveRoom(const UserPrx& user, const ::Ice::Current&) {
        for (auto i = users.begin(); i != users.end(); ++i) {
            if ((*i) == user) {
                //usuwa usera 
                i = users.erase(i);
                break;
            } 
        }
        cout << "Uzytkownik " << user->getName() << " wyszedl" << endl;
    }

    UserList RoomI::getUsers(const ::Ice::Current&) {
        return users;
    }

    void RoomI::AddUser(const UserPrx& user, const ::Ice::Current&) {
        users.push_back(user);
    }

    

    void RoomI::Destroy(const ::Ice::Current&) {
        //czysci liste
        users.clear();
    }

    void RoomI::SendMessage(const UserPrx& user, const string& message, const ::Ice::Current&) {
        for (auto& u : users) {
            u->SendMessage(roomName, user, message);
        }
    }
}