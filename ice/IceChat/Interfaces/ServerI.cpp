#include "ServerI.h"

namespace Interfaces {
    RoomPrx ServerI::CreateRoom(const string& name, const ::Ice::Current&) {
        for (auto &room : roomList) {
            if (room->getName() == name) {
                throw RoomAlreadyExist();
            }
        }
       
        if (roomFactoryList.empty()) {
            throw NoResourcesAvailable();
        }
        RoomFactoryPrx roomFactory = roomFactoryList.back();
        RoomPrx room = roomFactory->createRoom(name);
        roomList.push_back(room);
        cout << "Stworzono pokoj " << name << endl;
        return room;
    }

    RoomList ServerI::getRooms(const ::Ice::Current&) {
        return roomList;
    }

    RoomPrx ServerI::FindRoom(const string& name, const ::Ice::Current& ) {
        for (auto &room : roomList) {
            if (room->getName() == name) {
                return room;
            }
        }
        throw NoSuchRoomExist();   
    }

    void ServerI::RegisterRoomFactory(const RoomFactoryPrx& roomFactory, const ::Ice::Current&) {
        roomFactoryList.push_back(roomFactory);
        cout << "Nowy room factory " << endl;
    }

    void ServerI::UnregisterRoomFactory(const RoomFactoryPrx& roomFactory, const ::Ice::Current&) {
        for (auto i= roomFactoryList.begin(); i != roomFactoryList.end(); ) {
            if (*i == roomFactory) {
                i = roomFactoryList.erase(i);
            } else {
                ++i;
            }
        }
    }
}