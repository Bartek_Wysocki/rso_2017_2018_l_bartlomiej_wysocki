#include "RoomFactoryI.h"

namespace Interfaces {
    RoomList RoomFactoryI::getRooms(const ::Ice::Current&) {
        return roomList;
    }

    RoomPrx RoomFactoryI::createRoom(const string& name, const ::Ice::Current&) {
        RoomPtr object = new RoomI(name);

       
        int port = portsUtil.getRandomPort();

        adapter = ic->createObjectAdapterWithEndpoints("SimpleRoom" + name, "default -p " + to_string(port));
        adapter->add(object, ic->stringToIdentity("SimpleRoom" + name));
        adapter->activate();
        //zmiena string proxy na proxy
        Ice::ObjectPrx base = ic->stringToProxy("SimpleRoom" + name + ":default -p " + to_string(port));
        RoomPrx room = RoomPrx::checkedCast(base);

        //dodaje na koniec listy
        roomList.push_back(room);
        cout << "Nowy pokoj: " << name << endl;
        return room;
    }

    RoomFactoryI::RoomFactoryI() {
        //tworzy obiekt Ice
        ic = Ice::initialize();
    }

    RoomFactoryI::~RoomFactoryI() {
        if (ic) {
            try {
                //niszczy komunikator
                ic->destroy();
            } catch (const Ice::Exception& e) {
                cerr << e << endl;
            }
        }
    }
}