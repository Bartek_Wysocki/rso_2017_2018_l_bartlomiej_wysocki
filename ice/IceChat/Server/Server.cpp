#include <Ice/Ice.h>
#include "chat.h"
#include "ServerI.h"
#include "Ports.h"

using namespace Interfaces;
using namespace std;

int main(int argc, char* argv[]) {
    int s = 0;
    Ports portsUtil;
    Ice::CommunicatorPtr iceCommunicator;
    try {
        iceCommunicator = Ice::initialize(argc, argv);
        int port = portsUtil.getServerPort();
        //nazwa adapter , port 
        Ice::ObjectAdapterPtr adapter = iceCommunicator->createObjectAdapterWithEndpoints("ServerAdapter", "default -p " + to_string(port));
        Ice::ObjectPtr object = new ServerI();
        
        adapter->add(object, iceCommunicator->stringToIdentity("Server"));
        adapter->activate();
        
        iceCommunicator->waitForShutdown();
    } catch (const char* msg) {
        cerr << msg << endl;
        s = 1;
    } catch (const RoomAlreadyExist &ex) {
        cerr << ex << endl;
    } catch (const NoSuchRoomExist& ex) {
            cerr << ex << endl;
    } catch (const UserAlreadyExists& ex) {
            cerr << ex << endl;
    } catch (const Ice::Exception& e) {
        cerr << e << endl;
        s = 1;
    } 
    if (iceCommunicator) {
        try {
            iceCommunicator->destroy();
        } catch (const Ice::Exception& e) {
            cerr << e << endl;
            s = 1;
        }
    }
    return s;
}
